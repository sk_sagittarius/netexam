﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;

namespace ServerApp
{
    class Program
    {
        private static string number = RandomNumber();

        static void Main(string[] args)
        {
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                string ipServer = "127.0.0.1";
                int port = 12345;

                socket.Bind(new IPEndPoint(IPAddress.Parse(ipServer), port));
                socket.Listen(100);
                while(true)
                {
                    Socket client = socket.Accept();
                    ThreadPool.QueueUserWorkItem(ClientThreadProcedure, client);
                }
            }
        }


        static void ClientThreadProcedure(object obj)
        {
            Socket client = (Socket)obj;
            //Console.WriteLine(number); // для проверки загаданного числа
            try
            {
                client.Send(Encoding.UTF8.GetBytes(number), number.Length, SocketFlags.None);

                byte[] buf = new byte[4 * 1024];
                int recSize = client.Receive(buf);
                string command = Encoding.UTF8.GetString(buf, 0, recSize);
                if (command == "random")
                {
                    number = RandomNumber();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }



        private static string RandomNumber()
        {
            Random random = new Random();

            int first = random.Next(0, 9);
            int second;
            int third;
            int fourth;

            do
            {
                second = random.Next(0, 9);
            } while (second == first);

            do
            {
                third = random.Next(0, 9);
            } while (third == first || third == second);

            do
            {
                fourth = random.Next(0, 9);
            } while (fourth == first || fourth == second || fourth == third);

            string number = first.ToString() + second.ToString() + third.ToString() + fourth.ToString();
            return number;
        }
    }
}
