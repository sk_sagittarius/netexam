﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetExam
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Socket socket;

        public MainWindow()
        {
            InitializeComponent();
            Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindowClosing);
        }
        
        
        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            using (socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                string ipServer = "127.0.0.1";
                int port = 12345;
                var input = userTextBox.Text;

                socket.Connect(ipServer, port);
                IPHostEntry hostEntry = Dns.GetHostEntry(ipServer);

                if(socket.Connected)
                {
                    socket.Send(Encoding.UTF8.GetBytes(input));

                    byte[] buf = new byte[4 * 1024];
                    int recSize = socket.Receive(buf);


                    string number = Encoding.UTF8.GetString(buf, 0, recSize);
                    Logic(input, number);
                }
            }


            
        }

        private void Logic(string input, string number)
        {
            if (input.Distinct().Count() < 4)
            {
                MessageBox.Show("Цифры не должны повторяться!");
            }

            else
            {
                char[] unknownNumber = number.ToArray();
                char[] inputNumber = input.ToArray();

                int countCows = 0;
                int countBulls = 0;



                foreach (var i in inputNumber)
                {
                    for (int j = 0; j < unknownNumber.Length; j++)
                    {
                        if (i == unknownNumber[j])
                        {
                            countCows++;
                        }
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    if (inputNumber[i] == unknownNumber[i])
                    {
                        countBulls++;
                        countCows--;
                    }
                }


                stepsTextBox.Text += input + " быков - " + countBulls + ", коров - " + countCows + "\n";
                if (countBulls == 4)
                {
                    MessageBox.Show("Победа!");
                    startButton.IsEnabled = false;
                  
                }
            }
        }

        private void UserTextBoxPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !Char.IsDigit(e.Text, 0);
        }

        private void EndButtonClick(object sender, RoutedEventArgs e)
        {
            

            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                string ipServer = "127.0.0.1";
                int port = 12345;
                var command = "random";

                socket.Connect(ipServer, port);
                IPHostEntry hostEntry = Dns.GetHostEntry(ipServer);
                if (socket.Connected)
                {
                    socket.Send(Encoding.UTF8.GetBytes(command));

                    stepsTextBox.Text = "";
                    startButton.IsEnabled = true;

                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();

                }
            }
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
            Application.Current.Shutdown();
        }

        void MainWindowClosing(object sender, CancelEventArgs e)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

    }
}
